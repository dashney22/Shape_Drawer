package com.dashney.drawit.AutomatedDrawer.controller;

import com.dashney.drawit.AutomatedDrawer.entity.User;
import com.dashney.drawit.AutomatedDrawer.formModel.UserFormModel;
import com.dashney.drawit.AutomatedDrawer.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@Controller
public class AuthController {

    // Create a user service to store to database
    private UserService userService;


    // Hnadler to take me to the Index Page

    @GetMapping("/index")
    public String home(Model model){
        model.addAttribute("title","Index");
        return "index";
    }

    //Handler method to handle user registration form request

    @GetMapping("/register")
    public String showRegistration(Model model){
        UserFormModel user = new UserFormModel();
        model.addAttribute("user",user);
        model.addAttribute("title","registration");
        return "register";
    }
    @GetMapping("/draw")
    public String drawPage(Model model){
        List<UserFormModel> users = userService.findAllUsers();
        model.addAttribute("title","Draw");
        model.addAttribute("username", users.get(0).getFirstName());
        return "draw";
    }
    // Method to handle user registration form submit request
    @PostMapping("/register/save")
    public String registration(@Valid @ModelAttribute("user") UserFormModel userFormModel,
                               BindingResult result,
                               Model model){
        User existingUser = userService.findUserByEmail(userFormModel.getEmail());

        if(existingUser != null && existingUser.getEmail() != null && !existingUser.getEmail().isEmpty()){
            result.rejectValue("email", null,
                    "There is already an account registered with the same email");
        }

        if(result.hasErrors()){
            model.addAttribute("user", userFormModel);
            return "/register";
        }

        userService.saveUser(userFormModel);
        return "redirect:/register?success";
    }

    // Method to display all Registered Users

    @GetMapping("/users")
    public String users(Model model){
        List<UserFormModel> users = userService.findAllUsers();
        model.addAttribute("users",users);
        model.addAttribute("username", users.get(0).getFirstName());
        return "users";
    }

    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("title","Login");
        return "login";
    }
}
