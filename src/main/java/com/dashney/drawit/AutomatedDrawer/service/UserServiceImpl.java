package com.dashney.drawit.AutomatedDrawer.service;

import com.dashney.drawit.AutomatedDrawer.entity.Role;
import com.dashney.drawit.AutomatedDrawer.entity.User;
import com.dashney.drawit.AutomatedDrawer.formModel.UserFormModel;
import com.dashney.drawit.AutomatedDrawer.repository.RoleRepository;
import com.dashney.drawit.AutomatedDrawer.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService{
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Override
    public void saveUser(UserFormModel userFormModel) {
        User user = new User();
        user.setName(userFormModel.getFirstName() + " " + userFormModel.getLastName());
        user.setEmail(userFormModel.getEmail());

        // Encrypt the password
        user.setPassword(passwordEncoder.encode(userFormModel.getPassword()));

        Role role = roleRepository.findByName("ROLE_ADMIN");

        if(role== null){
            role = checkRoleExist();
        }

        user.setRoles(Arrays.asList(role));
        userRepository.save(user);
    }

    // Check if Role Exist
    private Role checkRoleExist() {
        Role role = new Role();
        role.setName("ROLE_ADMIN");
        return roleRepository.save(role);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    // Method that displays all users to the table
    @Override
    public List<UserFormModel> findAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map((user)->mapToUserDTO(user))
                .collect(Collectors.toList());
    }

    //private method
    private UserFormModel mapToUserDTO(User user){
        UserFormModel userFormModel = new UserFormModel();
        String[] str = user.getName().split(" ");
        userFormModel.setFirstName(str[0]);
        userFormModel.setLastName(str[1]);
        userFormModel.setEmail(user.getEmail());
        return userFormModel;
    }
}
