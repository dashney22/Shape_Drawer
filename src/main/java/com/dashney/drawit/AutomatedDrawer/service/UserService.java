package com.dashney.drawit.AutomatedDrawer.service;

import com.dashney.drawit.AutomatedDrawer.entity.User;
import com.dashney.drawit.AutomatedDrawer.formModel.UserFormModel;

import java.util.List;

public interface UserService {
    // Function to save user after filling form with data
    void saveUser(UserFormModel userFormModel);

    User findUserByEmail(String email);

    List<UserFormModel> findAllUsers();
}
