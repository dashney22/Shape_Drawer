package com.dashney.drawit.AutomatedDrawer.formModel;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserFormModel {

    private Long id;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty(message = "Email should not be empty")
    @Email
    private String email;
    @NotEmpty(message = "Password should be empty")
    private String password;

    public UserFormModel(String firstName,String lastName, String email, String password){
        this.firstName=firstName;
        this.lastName = lastName;
        this.email=email;
        this.password = password;
    }
}

