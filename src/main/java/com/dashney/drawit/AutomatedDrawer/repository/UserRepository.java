package com.dashney.drawit.AutomatedDrawer.repository;

import com.dashney.drawit.AutomatedDrawer.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
