package com.dashney.drawit.AutomatedDrawer.repository;

import com.dashney.drawit.AutomatedDrawer.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
